import { ActorSheetPFCharacter, ItemChange } from "../../../../systems/pf1/pf1.js";
import { settings } from "../costants.js";
import { templates } from "../main.js";
import { utils } from "../utils.js";

export class GoogleCharacterSheet extends ActorSheetPFCharacter {

    get template() {
        return templates.googleCharacterSheet.main;
    }

    activateListeners(html) {
        super.activateListeners(html);
        html.find(".refresh").click(this.updateData.bind(this));
    }

    async getData() {
        const data = await super.getData();    
        if (data.actor.data.options) data.siteUrl = data.actor.data.options.sheetLink;
        data.compat = false;
        return data;
    }

    async updateData(event) {
        event.preventDefault();
        //TODO disable refresh button
        console.log("Start");

        google.clientId = utils.getSetting(settings.google.clientId);
        google.apiKey = utils.getSetting(settings.google.apiKey);

        let regex = /\/d\/(?<id>[^\/]+)\//;
        let loadData = await this.getData();
        let sheetLink = loadData.actor.data.data.options.sheetLink;
        let sheetId = sheetLink.match(regex).groups.id;
        let actorSheet = this;

        this.ptrintPulledData("Reading");
        google.loadAuth(async function(isSignedIn) {
            if (isSignedIn) {
                //let actorData = actorSheet.actor.data.data;
                let promise = null;
                let sheetData = {};
                let abilityScoreData = {};
                let actorClassData = {};
                let skillData = {};
                let miscData = {};
                let resultString = "Loading";
                let actorClassStartData = { // maybe this need to be revised
                        "data.bab": "",
                        "data.hp": 0,
                        "data.hd": 0,
                        "data.savingThrows.fort.value": "",
                        "data.savingThrows.ref.value": "",
                        "data.savingThrows.will.value": "",
                    }
                    //pull scores & hp

                try {
                    let maxRequests = 12;
                    let requestDelay = 5000;

                    let isRequestNotValid = true;
                    let requestCount = 0;

                    while (isRequestNotValid) {
                        promise = await google.readFromSpeadsheet(sheetId, "StrScore,DexScore,ConScore,IntScore,WisScore,ChaScore,CharacterLevel,MaxHP,SkillBonuses,Initiative,Will,Reflex,Fortitude,TotalBAB");
                        resultString = JSON.stringify(promise.result);
                        isRequestNotValid = resultString.includes("Loading");
                        requestCount++;

                        if (requestCount >= maxRequests) {
                            throw "Google failed to calulate sheet values";
                        }
                        if (isRequestNotValid) {
                            actorSheet.ptrintPulledData("Waiting for google sheet to complete caclulations");
                            await actorSheet.sleep(requestDelay);
                        }
                    }
                } catch (error) {
                    let message = error.body;
                    if (message.includes("parse")) {
                        let regex = /range: (?<range>[^"]+)/m
                        let match = message.match(regex);
                        console.log(match);
                        actorSheet.ptrintPulledData("Erroe: your sheet is missing the '" + match.groups.range + "' range.\nPlease add that range otherwise the sheet cannot be updated.");
                    } else {
                        actorSheet.ptrintPulledData(message);
                    }
                    return;
                }

                actorSheet.ptrintPulledData(resultString);
                console.log(promise.result);

                //grab sheet data
                //abilities
                sheetData.str = promise.result.valueRanges[0].values[0][0];
                sheetData.dex = promise.result.valueRanges[1].values[0][0];
                sheetData.con = promise.result.valueRanges[2].values[0][0];
                sheetData.int = promise.result.valueRanges[3].values[0][0];
                sheetData.wis = promise.result.valueRanges[4].values[0][0];
                sheetData.cha = promise.result.valueRanges[5].values[0][0];

                //skills
                sheetData.skills = {};
                sheetData.skills.acrobatic = promise.result.valueRanges[8].values[0][0];
                sheetData.skills.appraise = promise.result.valueRanges[8].values[1][0];
                sheetData.skills.autohypnosis = promise.result.valueRanges[8].values[2][0];
                sheetData.skills.bluff = promise.result.valueRanges[8].values[3][0];
                sheetData.skills.climb = promise.result.valueRanges[8].values[4][0];
                sheetData.skills.diplomacy = promise.result.valueRanges[8].values[5][0];
                sheetData.skills.disabledevice = promise.result.valueRanges[8].values[6][0];
                sheetData.skills.disguise = promise.result.valueRanges[8].values[7][0];
                sheetData.skills.escapeartist = promise.result.valueRanges[8].values[8][0];
                sheetData.skills.fly = promise.result.valueRanges[8].values[9][0];
                sheetData.skills.handleanimal = promise.result.valueRanges[8].values[10][0];
                sheetData.skills.heal = promise.result.valueRanges[8].values[11][0];
                sheetData.skills.intimidate = promise.result.valueRanges[8].values[12][0];
                sheetData.skills.arcana = promise.result.valueRanges[8].values[13][0];
                sheetData.skills.dungeoneering = promise.result.valueRanges[8].values[14][0];
                sheetData.skills.engineering = promise.result.valueRanges[8].values[15][0];
                sheetData.skills.geography = promise.result.valueRanges[8].values[16][0];
                sheetData.skills.history = promise.result.valueRanges[8].values[17][0];
                sheetData.skills.local = promise.result.valueRanges[8].values[18][0];
                sheetData.skills.nature = promise.result.valueRanges[8].values[19][0];
                sheetData.skills.nobility = promise.result.valueRanges[8].values[20][0];
                sheetData.skills.planes = promise.result.valueRanges[8].values[21][0];
                sheetData.skills.psionics = promise.result.valueRanges[8].values[22][0];
                sheetData.skills.religion = promise.result.valueRanges[8].values[23][0];
                sheetData.skills.linguistics = promise.result.valueRanges[8].values[24][0];
                sheetData.skills.perception = promise.result.valueRanges[8].values[25][0];
                sheetData.skills.ride = promise.result.valueRanges[8].values[26][0];
                sheetData.skills.sensemotive = promise.result.valueRanges[8].values[27][0];
                sheetData.skills.sleightofhand = promise.result.valueRanges[8].values[28][0];
                sheetData.skills.spellcraft = promise.result.valueRanges[8].values[29][0];
                sheetData.skills.stealth = promise.result.valueRanges[8].values[30][0];
                sheetData.skills.survival = promise.result.valueRanges[8].values[31][0];
                sheetData.skills.swim = promise.result.valueRanges[8].values[32][0];
                sheetData.skills.usemagicdevice = promise.result.valueRanges[8].values[33][0];
                sheetData.skills.skill1 = promise.result.valueRanges[8].values[34][0];
                sheetData.skills.skill2 = promise.result.valueRanges[8].values[35][0];
                sheetData.skills.skill3 = promise.result.valueRanges[8].values[36][0];
                sheetData.skills.skill4 = promise.result.valueRanges[8].values[37][0];
                sheetData.skills.skill5 = promise.result.valueRanges[8].values[38][0];
                sheetData.skills.skill6 = promise.result.valueRanges[8].values[39][0];

                //misc
                sheetData.concentration = promise.result.valueRanges[8].values[40][0];
                sheetData.characterLevel = promise.result.valueRanges[6].values[0][0];
                sheetData.hp = promise.result.valueRanges[7].values[0][0];
                sheetData.initiative = promise.result.valueRanges[9].values[0][0];
                sheetData.bab = promise.result.valueRanges[13].values[0][0];

                //saves
                sheetData.will = promise.result.valueRanges[10].values[0][0];
                sheetData.reflex = promise.result.valueRanges[11].values[0][0];
                sheetData.fortitude = promise.result.valueRanges[12].values[0][0];

                //defences
                // sheetData.ac = promise.result.valueRanges[13].values[0][0];
                // sheetData.flatfooted = promise.result.valueRanges[14].values[0][0];
                // sheetData.touchac = promise.result.valueRanges[15].values[0][0];
                // sheetData.cmd = promise.result.valueRanges[16].values[0][0];

                // update abilities data
                abilityScoreData["data.abilities.str.value"] = sheetData.str;
                abilityScoreData["data.abilities.dex.value"] = sheetData.dex;
                abilityScoreData["data.abilities.con.value"] = sheetData.con;
                abilityScoreData["data.abilities.int.value"] = sheetData.int;
                abilityScoreData["data.abilities.wis.value"] = sheetData.wis;
                abilityScoreData["data.abilities.cha.value"] = sheetData.cha;
                await actorSheet.actor.update(abilityScoreData);

                let actorData = actorSheet.actor.data.data;
                //update class data
                console.log(actorSheet.actor.data);
                sheetData.hp = sheetData.hp - actorData.abilities.con.mod * sheetData.characterLevel;

                let actorClass = await actorSheet.getSheetClass();
                await actorClass.update(actorClassStartData);
                let will = await actorSheet.createBuffChange("" + (sheetData.will - actorData.abilities.wis.mod), "untyped", "add", "will");
                let fort = await actorSheet.createBuffChange("" + (sheetData.fortitude - actorData.abilities.con.mod), "untyped", "add", "fort");
                let refl = await actorSheet.createBuffChange("" + (sheetData.reflex - actorData.abilities.dex.mod), "untyped", "add", "ref");
                let bab = await actorSheet.createBuffChange("" + sheetData.bab, "untyped", "add", "bab");

                let changes = [will.data, fort.data, refl.data, bab.data];


                actorClassData["data.level"] = sheetData.characterLevel;
                actorClassData["data.hp"] = sheetData.hp;
                actorClassData["data.changes"] = changes;
                await actorClass.update(actorClassData);

                //update skills
                skillData["data.skills.acr.rank"] = sheetData.skills.acrobatic - actorData.abilities.dex.mod;
                skillData["data.skills.apr.rank"] = sheetData.skills.appraise - actorData.abilities.int.mod;
                //skillData["data.skills.acr"] = sheetData.skills.autohypnosis;
                skillData["data.skills.blf.rank"] = sheetData.skills.bluff - actorData.abilities.cha.mod;
                skillData["data.skills.clm.rank"] = sheetData.skills.climb - actorData.abilities.str.mod;
                skillData["data.skills.dip.rank"] = sheetData.skills.diplomacy - actorData.abilities.cha.mod;
                skillData["data.skills.dev.rank"] = sheetData.skills.disabledevice - actorData.abilities.dex.mod;
                skillData["data.skills.dis.rank"] = sheetData.skills.disguise - actorData.abilities.cha.mod;
                skillData["data.skills.esc.rank"] = sheetData.skills.escapeartist - actorData.abilities.dex.mod;
                skillData["data.skills.fly.rank"] = sheetData.skills.fly - actorData.abilities.dex.mod;
                skillData["data.skills.han.rank"] = sheetData.skills.handleanimal - actorData.abilities.cha.mod;
                skillData["data.skills.hea.rank"] = sheetData.skills.heal - actorData.abilities.wis.mod;
                skillData["data.skills.int.rank"] = sheetData.skills.intimidate - actorData.abilities.cha.mod;
                skillData["data.skills.kar.rank"] = sheetData.skills.arcana - actorData.abilities.int.mod;
                skillData["data.skills.kdu.rank"] = sheetData.skills.dungeoneering - actorData.abilities.int.mod;
                skillData["data.skills.ken.rank"] = sheetData.skills.engineering - actorData.abilities.int.mod;
                skillData["data.skills.kge.rank"] = sheetData.skills.geography - actorData.abilities.int.mod;
                skillData["data.skills.khi.rank"] = sheetData.skills.history - actorData.abilities.int.mod;
                skillData["data.skills.klo.rank"] = sheetData.skills.local - actorData.abilities.int.mod;
                skillData["data.skills.kna.rank"] = sheetData.skills.nature - actorData.abilities.int.mod;
                skillData["data.skills.kno.rank"] = sheetData.skills.nobility - actorData.abilities.int.mod;
                skillData["data.skills.kpl.rank"] = sheetData.skills.planes - actorData.abilities.int.mod;
                // skillData["data.skills.acr"] = sheetData.skills.psionics;
                skillData["data.skills.kre.rank"] = sheetData.skills.religion - actorData.abilities.int.mod;
                skillData["data.skills.lin.rank"] = sheetData.skills.linguistics - actorData.abilities.int.mod;
                skillData["data.skills.per.rank"] = sheetData.skills.perception - actorData.abilities.wis.mod;
                skillData["data.skills.rid.rank"] = sheetData.skills.ride - actorData.abilities.dex.mod;
                skillData["data.skills.sen.rank"] = sheetData.skills.sensemotive - actorData.abilities.wis.mod;
                skillData["data.skills.slt.rank"] = sheetData.skills.sleightofhand - actorData.abilities.dex.mod;
                skillData["data.skills.spl.rank"] = sheetData.skills.spellcraft - actorData.abilities.int.mod;
                skillData["data.skills.ste.rank"] = sheetData.skills.stealth - actorData.abilities.dex.mod;
                skillData["data.skills.sur.rank"] = sheetData.skills.survival - actorData.abilities.wis.mod;
                skillData["data.skills.swm.rank"] = sheetData.skills.swim - actorData.abilities.str.mod;
                skillData["data.skills.umd.rank"] = sheetData.skills.usemagicdevice - actorData.abilities.cha.mod;
                // skillData["data.skills.acr"] = sheetData.skills.skill1;
                // skillData["data.skills.acr"] = sheetData.skills.skill2;
                // skillData["data.skills.acr"] = sheetData.skills.skill3;
                // skillData["data.skills.acr"] = sheetData.skills.skill4;
                // skillData["data.skills.acr"] = sheetData.skills.skill5;
                // skillData["data.skills.acr"] = sheetData.skills.skill6;
                await actorSheet.actor.update(skillData);

                //update misc data;
                miscData["data.attributes.init.value"] = sheetData.initiative - actorData.abilities.dex.mod;

                //miscData[""] = sheetData.concentration - sheetData.characterLevel;
                await actorSheet.actor.update(miscData);

                console.log(skillData);
                console.log(actorClassData);
                console.log(sheetData);
                actorSheet.ptrintPulledData("done!");
            } else {
                google.requestPermission();
                actorSheet.ptrintPulledData("Done Reqeusting permission, click update one more time!");
            }
        });
    }

    async ptrintPulledData(text) {
        document.getElementById("pulled-data-info").innerText = text;
    }

    sleep(ms) {
        return new Promise(resolve => setTimeout(resolve, ms));
    }

    async getSheetClass() {
        let actorClass = null;
        this.actor.items.forEach(item => {
            if (item.data.name == "sheetclass") {
                actorClass = item;
                return;
            }
        });
        if (actorClass == null) {
            actorClass = await this.createSheetClass();
        }
        return actorClass;
    }

    async createSheetClass() {
        const itemData = {
            name: "sheetclass",
            type: "class",
            data: {}
        };
        await this.document.createEmbeddedDocuments("Item", [itemData]);

        let actorClass = null;
        this.actor.items.forEach(item => {
            if (item.data.name == "sheetclass") {
                actorClass = item;
                return;
            }
        });
        if (actorClass == null){
            throw "Could not create Actor Class"
        }
        return actorClass;
    }

    async createBuffChange(formula, modifier, operator, subTarget,priority=0) {
        let change = ItemChange.create({}, null);
        change.data.formula = formula;
        change.data.modifier = modifier;
        change.data.operator = operator;
        //change.data.target = target;
        change.data.subTarget = subTarget;
        change.data.priority = priority;
        change.data.value = formula;
        return change;
    }
}

var google = {

    clientId: "",
    scope: "https://www.googleapis.com/auth/spreadsheets.readonly",
    apiKey: "",

    loadAuth(onResponseRecived) {
        gapi.load('client:auth2', () => {
            gapi.client.init({
                'apiKey': this.apiKey,
                'clientId': this.clientId,
                'scope': this.scope,
                'discoveryDocs': ['https://sheets.googleapis.com/$discovery/rest?version=v4'],
            }).then(function() {
                let isSignedIn = gapi.auth2.getAuthInstance().isSignedIn.get();
                onResponseRecived(isSignedIn);
                gapi.auth2.getAuthInstance().isSignedIn.listen(onResponseRecived);

            }).catch(function() {
                console.error("cannot init");
            });
        });

    },

    requestPermission() {
        gapi.auth2.getAuthInstance().signIn();
    },

    readFromSpeadsheet(spreadsheetId, ranges, onResponseRecived) {
        let params = {
            spreadsheetId: spreadsheetId,
            ranges: ranges.split(','),
        };
        return gapi.client.sheets.spreadsheets.values.batchGet(params);
    }
};