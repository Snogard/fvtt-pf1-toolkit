import { hookType } from "../../fvtt-library/src/foundrylib.js";
import { GoogleCharacterSheet } from "./actors/googleCharacterSheet.js";

Hooks.once(hookType.module.init, () => {
    RegisterSheets();
});


function RegisterSheets() {
    loadTemplates(templates.getArray());

    Actors.registerSheet("PF1", GoogleCharacterSheet, {
        types: ["character"],
        makeDefault: false
    });
}

export let templates = {
    googleCharacterSheet: {
        main: "modules/fvtt-pf1-tools/templates/actors/google-character-sheet.hbs",
        inlineWebViewer: "modules/inlinewebviewer/templates/inlineViewer.html",
        skills: "modules/fvtt-pf1-tools/templates/actors/parts/actor-skills.hbs",
        scores: "modules/fvtt-pf1-tools/templates/actors/parts/actor-scores.hbs",
        savingThrows: "modules/fvtt-pf1-tools/templates/actors/parts/actor-savingthrows.hbs",
        attacks: "modules/fvtt-pf1-tools/templates/actors/parts/actor-attacks.hbs"
    },

    getArray() {
        return [
            this.googleCharacterSheet.main,
            this.googleCharacterSheet.inlineWebViewer,
            this.googleCharacterSheet.skills,
            this.googleCharacterSheet.scores,
            this.googleCharacterSheet.savingThrows,
            this.googleCharacterSheet.attacks,
        ]
    }
}