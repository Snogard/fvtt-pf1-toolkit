import { hookType, scopeType } from "../../fvtt-library/src/foundrylib.js";
import { settings } from "./costants.js";

Hooks.once(hookType.module.init, registerSettings);

function registerSettings() {
    game.settings.register(settings.name, settings.google.apiKey, {
        name: "Google Api Key",
        hint: "",
        default: "",
        type: String,
        scope: scopeType.world,
        config: true,
    });

    game.settings.register(settings.name, settings.google.clientId, {
        name: "Google client ID",
        hint: "",
        default: "",
        type: String,
        scope: scopeType.world,
        config: true,
    });
}