import { utils } from "../utils.js"


export let creatureReader = {
    readCreature(html) {
        let creatureData = {};
        creatureData.skills = {};
        creatureData.senses = {};
        creatureData.abilityScores = {};

        let result = {};

        html = utils.getSection(html);

        result = getShortDescription(html);
        creatureData.shortDescription = result.shortDescription;
        html = html.substring(result.end);

        result = getNameAndCR(html);
        creatureData.name = result.name;
        creatureData.cr = result.cr;
        html = html.substring(result.end);

        //TODO: get race and class

        result = getAlignmentAndSizeAndCreatureType(html);
        creatureData.alignment = result.alignment;
        creatureData.size = result.size;
        creatureData.type= result.creatureType;
        creatureData.typeLink= result.creatureTypeLink;
        creatureData.subType= result.subCreatureType;
        creatureData.subTypeLink= result.subCreatureTypeLink;
        html = html.substring(result.end);

        


        console.log(creatureData);
        console.log(html);

        return creatureData
    }

}

function getInitiativeAndSensesAndPerception(){

}


function getAlignmentAndSizeAndCreatureType(html){ 
    let result = {};
    let regex = /(?<alignment>LG|NG|CG|LN|N|CN|LE|NE|CE) (?<size>Fine|Diminutive|Tiny|Small|Medium|Large|Huge|Gargantuan|Colossal) (<a href="(?<linkCreatureType>[a-zA-Z:\/\.0-9-#]+)">)?(?<creatureType>[a-z]+)(<\/a>)?( \((<a href="(?<linkSubCreatureType>[a-zA-Z:\/\.0-9-#]+)">)?(?<subCreatureType>[a-z]+)(<\/a>)?\))?/
    let content = html.substring(0, html.length);
    let match = content.match(regex);

    result.alignment= match.groups.alignment;
    result.size=match.groups.size;
    result.creatureType=match.groups.creatureType;
    result.creatureTypeLink=match.groups.creatureTypeLink;
    result.subCreatureType=match.groups.subCreatureType;
    result.subCreatureTypeLink=match.groups.subCreatureTypeLink;

    result.end = match.index + match[0].length;

    return result;

}

function getRaceAndClass() {}


function getNameAndCR()
{
    let result = {};
    let regex = /<p class="title">(?<name>[a-zA-Z0-9 ,\.]+)<span class="level">CR (?<cr>[0-9\/]+)<\/span>/
    let content = html.substring(0, html.length);
    let match = content.match(regex);

    result.name = match.groups.name;
    result.cr= match.groups.cr;
    result.end = match.index + match[0].length;

    return result;
}

function getShortDescription(html){
    let result = {};
    let regex = /<p class="description">(?<shortDescription>[a-zA-Z ,\.]+)<\/p>/
    let content = html.substring(0, html.length);
    let match = content.match(regex);

    result.shortDescription = match.groups.shortDescription;
    result.end = match.index + match[0].length;

    return result;
}