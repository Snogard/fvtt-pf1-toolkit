import { utils } from "../utils.js";

export let spellReader = {
    readSpell(html) { //todo import more than one spell
        let spellData = {};
        let result = {};

        html = utils.getSection(html);

        result = getName(html);
        spellData.name = result.name;
        html = html.substring(result.end);

        result = getSchoolInfo(html);
        spellData.school = result.school;
        spellData.subSchool = result.subSchool;
        spellData.descriptor = result.descriptor;

        html = utils.getArticle(html);

        result = getLevels(html);
        spellData.levels = result.levels;
        html = html.substring(result.end);

        result = getDomains(html);
        if (result.domains.length > 0) spellData.domains = result.domains;
        html = html.substring(result.end);

        result = getCastingTime(html);
        spellData.castingTime = result.castingTime;
        html = html.substring(result.end);

        result = getComponents(html);
        spellData.components = result.components;
        html = html.substring(result.end);

        result = getEffect(html);
        spellData.effect = result.effect;
        html = html.substring(result.end);

        result = getDescription(html);
        spellData.description = result.description;
        html = html.substring(result.end);

        result = getSubSpells(html);
        spellData.subSpells = result.subSpells;
        // html.substring(result.end);


        return spellData;
    }
}

function getSubSpells(html) {
    let result = {};
    let regex = /<h4>/g
    let match = [...html.matchAll(regex)];

    let numberOfSpells = match != null ? match.length : 0;
    if (numberOfSpells > 0) { result.subSpells = []; }

    for (let i = 0; i < numberOfSpells; i++) {
        let subSpell = {};
        let subRes = {};

        subRes = getSubSpellName(html);
        subSpell.name = subRes.name;
        console.log(subSpell.name);
        html = html.substring(subRes.end);

        subRes = getSchoolInfo(html);
        subSpell.school = subRes.school;
        subSpell.subSchool = subRes.subSchool;
        subSpell.descriptor = subRes.descriptor;
        html = html.substring(subRes.end);

        subRes = getLevels(html);
        subSpell.levels = subRes.levels;
        html = html.substring(subRes.end);

        subRes = getDomains(html);
        if (subRes.domains.length > 0) subSpell.domains = subRes.domains;
        html = html.substring(subRes.end);

        subRes = getSubSpellCastingTime(html);
        subSpell.castingTime = subRes.castingTime;

        subRes = getComponents(html);
        subSpell.components = subRes.components;
        //html = html.substring(subRes.end);

        console.log(html);
        subRes = getSubSpellEffect(html);
        if (subRes.effect) subSpell.effect = subRes.effect;
        //html = html.substring(subRes.end);

        subRes = getSubSpellDescription(html);
        subSpell.description = subRes.description;
        html = html.substring(subRes.end);

        result.subSpells.push(subSpell);
    }
    return result;
}

function getSubSpellDescription(html) {
    let result = {};
    let regex = /(?<description>(((<p>.*<\/p>)([ \n\r]+)?)+)([ <>a-zA-Z\"\=0-9"\n\r\/\(\),;:.*\#\&\-]+))<h4/m;
    let start = html.indexOf("DESCRIPTION</p>") + "DESCRIPTION</p>".length;
    let end = html.length;
    let content = html.substring(start, end);
    content = content.substring(content.indexOf("<p>"));
    let match = content.match(regex);

    //console.log(content);
    if (match) {
        result.description = match.groups.description.replace(/( \r| \n|\n|\r)?/g, "").trim();
        result.description = result.description.substring(0, result.description.lastIndexOf("</p>") + "</p>".length);
        result.end = html.indexOf("<h4");
    }
    return result;

}

function getSubSpellEffect(html) {
    let result = {};
    let regex = /<p>(<b>Range<\/b>( )+(?<range>[^<]+))?(.+<b>Target<\/b>( )+(?<target>[^<]+))?(.+<b>Effect<\/b>( )+(?<effect>[^<]+))?(.+<b>Duration<\/b>( )+(?<duration>[^<]+))(.+<b>Saving Throw<\/b>( )+(?<save>.+);)?((\D)+<b>Spell Resistance<\/b>( )+(?<resistance>[^<]+))?<\/p>/m;
    let start = 0;
    let end = html.indexOf("<h4>");
    if (end < 0) end = html.length;
    let content = html.substring(start, end);
    let match = content.match(regex);
    let effect = {};
    if (match) {
        effect.range = match.groups.range;
        if (match.groups.target) effect.target = match.groups.target;
        if (match.groups.duration) effect.duration = match.groups.duration;
        if (match.groups.save) effect.save = RemoveLinks(match.groups.save);
        if (match.groups.resistance) effect.resistance = match.groups.resistance;
        if (match.groups.effect) effect.effect = match.groups.effect;
        result.effect = effect;
    }

    // result.end = match[0].length;
    return result;
}

function getSubSpellCastingTime(html) {
    let result = {};
    let regex = /Casting Time<\/b>( )+(?<quantity>\d)( )+(<a href=\"[^"]+\">)?(?<action>\w+( )?(\w+)?)(<\/a>)?/m;
    let start = 0;
    let end = html.indexOf("</p>") + "</p>".length;
    let content = html.substring(start, end);
    let match = content.match(regex);

    if (match != null) {
        result.castingTime = match.groups.quantity.concat(" ").concat(match.groups.action);
    }

    return result;
}

function getSubSpellName(html) {
    let result = {};
    let regex = /<h4><span id=\"[a-zA-Z_]+\"><a name=\"[a-zA-Z\-]+\"><\/a>(?<name>[ a-zA-Z,]+)<\/span><\/h4>/m;
    let start = 0;
    let end = html.length;
    let content = html.substring(start, end);
    let match = content.match(regex);

    result.name = match.groups.name;
    result.end = match[0].length + match.index;

    return result;
}

function getDescription(html) {
    let result = {};
    let regex = /(?<description>(((<p>.*<\/p>)([ \n\r]+)?)+)([ <>a-zA-Z\"\=0-9"\n\r\/\(\),;:.*\#\&\-]+))<div/m;
    let start = html.indexOf("DESCRIPTION</p>") + "DESCRIPTION</p>".length;
    let end = html.length;
    let content = html.substring(start, end);
    content = content.substring(content.indexOf("<p>"));
    let match = content.match(regex);
    result.description = match.groups.description.replace(/( \r| \n|\n|\r)?/g, "").trim();
    result.description = result.description.substring(0, result.description.lastIndexOf("</p>") + "</p>".length);
    result.end = match.groups.description.length + start;
    return result;
}

function getEffect(html) {
    let result = {};
    let regex = /<p>(<b>Range<\/b>( )+(?<range>[^<]+))?(.+<b>Target<\/b>( )+(?<target>[^<]+))?(.+<b>Effect<\/b>( )+(?<effect>[^<]+))?(.+<b>Duration<\/b>( )+(?<duration>[^<]+))(.+<b>Saving Throw<\/b>( )+(?<save>.+);)?((\D)+<b>Spell Resistance<\/b>( )+(?<resistance>[^<]+))?<\/p>/m;
    let start = 0;
    let end = html.length;
    let content = html.substring(start, end);
    let match = content.match(regex);
    let effect = {};
    effect.range = match.groups.range;
    if (match.groups.target) effect.target = match.groups.target;
    if (match.groups.duration) effect.duration = match.groups.duration;
    if (match.groups.save) effect.save = RemoveLinks(match.groups.save);
    if (match.groups.resistance) effect.resistance = match.groups.resistance;
    if (match.groups.effect) effect.effect = match.groups.effect;
    result.effect = effect;
    // result.end = match[0].length;
    return result;
}

function getComponents(html) {
    let result = {};
    let regex = /Components<\/b>( )+(?<components>((V|S|DF|M( )+\([^)]+\))(,)?( )?)+)/;
    let start = 0;
    let end = html.indexOf("</p>", start);
    let content = html.substring(start, end);
    let match = content.match(regex);
    if (match != null) {
        result.components = match.groups.components.split(",");
        for (let i = 0; i < result.components.length; i++) {
            result.components[i] = result.components[i].trim();
        }
    }
    result.end = end;
    return result;
}

function getCastingTime(html) {
    let result = {};
    let regex = /Casting Time<\/b>( )+(?<quantity>\d)( )+(<a href=\"[^"]+\">)?(?<action>\w+( )?(\w+)?)(<\/a>)?/;
    let start = html.indexOf("Casting Time");
    let end = html.indexOf("</p>", start);
    let content = html.substring(start, end);
    let match = content.match(regex);
    result.castingTime = match.groups.quantity.concat(" ").concat(match.groups.action);
    result.end = match.index + start;
    return result;
}

function getDomains(html) {
    let result = {};
    let regex = /(<a href=\"[^"]+\">(?<domain>\w+(\/)?( )?(\w+)?)<\/a>( )+(?<level>\d))/g
    let domains = [];
    let start = html.indexOf("Domain");
    let end = html.indexOf("</p>", start) + "</p>".length;
    let content = html.substring(start, end);
    let match = [...content.matchAll(regex)];

    for (let i = 0; i < match.length; i++) {
        let currentMatch = match[i].groups;
        let domain = {};
        domain.name = currentMatch.domain;
        domain.level = currentMatch.level;
        domains.push(domain);
    }
    result.domains = domains;
    result.end = end;
    return result;
}

function getLevels(html) {
    let result = {};
    let regex = /(<a href=\"[^"]+\">(?<class>\w+(\/)?( )?(\w+)?)<\/a>( )+(?<level>\d))/g
    let levels = [];
    let start = 0;
    let end = html.indexOf("Domain", start);
    if (end < 0) end = html.indexOf("</p>") + "</p>".length;
    let content = html.substring(start, end);
    let match = [...content.matchAll(regex)];
    for (let i = 0; i < match.length; i++) {
        let currentMatch = match[i].groups;
        let level = {};
        level.class = currentMatch.class;
        level.value = currentMatch.level;
        levels.push(level);
    }
    result.levels = levels;
    result.end = end;
    return result;
}

function getSchoolInfo(html) {
    let result = {};
    let regex = /<b>School<\/b>( )+<a href=\"[^"]+">(?<school>\w+)<\/a>(( )+\(<a href=\"[^"]+\">(?<subSchool>\w+)<\/a>\))?(( )+\[(?<descriptor>\w+)\])?/
    let content = html.substring(0, html.length);
    let match = content.match(regex);

    result.school = match.groups.school;
    result.subSchool = match.groups.subSchool;
    result.descriptor = match.groups.descriptor;

    result.end = 0;
    return result;
}

function getName(html) {
    let result = {};
    let regex = /<h1>(?<name>[^<]+)<\/h1>/
    let content = html.substring(0, html.length);
    let match = content.match(regex);

    result.name = match.groups.name;
    result.end = 0;
    return result;
}

function RemoveLinks(text) {
    let regex = /(?<all><a href=\"[^"]+\">(?<text>[^<]+)<\/a>)/g;
    let match = [...text.matchAll(regex)]
    for (let i = 0; i < match.length; i++) {
        let currentMatch = match[i].groups;
        text = text.replace(currentMatch.all, currentMatch.text);
    }
    return text;
}
//#endregion
//#endregion