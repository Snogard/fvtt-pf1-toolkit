import {} from "https://apis.google.com/js/api.js";
import { settings } from "./costants.js";
const corsProxy = "https://cors-anywhere.herokuapp.com/"

export let utils = {
    getWebPageSync(path) {
        let httpRequest = new XMLHttpRequest();
        httpRequest.open("GET", path, false);
        httpRequest.send();
        return httpRequest.responseText;
    },

    getCorsWebPageSync(link) {
        let httpRequest = new XMLHttpRequest();
        httpRequest.open("GET", corsProxy + link, false);
        httpRequest.send();
        return httpRequest.responseText;
    },

    getArticle(html) {
        let start = html.indexOf("<article");
        let end = html.indexOf("</article>") + "</article>".length
        return html.substring(start, end);
    },

    getSection(html) {
        let start = html.indexOf("<section>");
        let end = html.indexOf("</section>") + "</section>".length
        return html.substring(start, end);
    },

    getSetting(name) {
        return game.settings.get(settings.name, name);
    },

    log(type, text) {
        console.log("pf1 tools | ".concat(type, ": ", text));
    },

}