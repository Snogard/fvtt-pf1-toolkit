import { utils } from "../../src/utils.js";
import { creatureReader } from "../../src/importers/creatureReader.js";

const expect = chai.expect;

describe("CreatureReader",()=>{
    describe("Kobold",()=> { //https://www.d20pfsrd.com/bestiary/monster-listings/humanoids/kobold/
        checkCreature("kobold"); 
    })

});

function checkCreature(name){
    let paths = GetCreatureData(name)

    let html = utils.getWebPageSync(paths.html);
    let json = JSON.parse(utils.getWebPageSync(paths.json));

    let result = {};
    let readError = null;

    try {
        result = creatureReader.readCreature(html);
    } catch (error) {
        readError = error;
        console.log(error);
    }

    it("exception", () => {
        expect(readError).to.equal(null);
    });

    it("name", () => {
        expect(result.name).to.equal(json.name);
    });
    it("short description", () => {
        expect(result.shortDescription).to.equal(json.shortDescription);
    });
    it("cr", () => {
        expect(result.cr).to.equal(json.cr);
    });
}



function GetCreatureData(name) {
    let file = "./suite/creatureReaderData/".concat(name);
    return { html: file.concat(".html"), json: file.concat(".json") }
}