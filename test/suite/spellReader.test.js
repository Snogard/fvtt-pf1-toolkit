import { utils } from "../../src/utils.js";
import { spellReader } from "../../src/importers/spellReader.js";

const expect = chai.expect;

describe("SpellReader", () => {
    describe("Spell Immunity", () => { //https://www.d20pfsrd.com/magic/all-spells/s/spell-immunity/
        checkSpell("spellImmunity"); //todo add subspells
    });

    describe("Desperate Weapon", () => { //https://www.d20pfsrd.com/magic/all-spells/d/desperate-weapon
        checkSpell("desperateWeapon");
    });

    describe("Rite Of Centered Mind", () => { //https://www.d20pfsrd.com/magic/all-spells/r/rite-of-centered-mind
        checkSpell("riteOfCenteredMind");
    });

    describe("Prismatic Spray", () => { //https://www.d20pfsrd.com/magic/all-spells/p/prismatic-spray
        checkSpell("prismaticSpray");
    });

    // describe("summon monster", () => { //https://www.d20pfsrd.com/magic/all-spells/s/summon-monster/
    //     checkSpell("summonMonster");
    // });

});

function checkSpell(name) {
    let paths = GetSpellData(name);

    let html = utils.getWebPageSync(paths.html);
    let json = JSON.parse(utils.getWebPageSync(paths.json));

    let result = {};
    let readError = null;

    try {
        result = spellReader.readSpell(html);
    } catch (error) {
        readError = error;
        console.log(error);
    }

    it("exception", () => {
        expect(readError).to.equal(null);
    });
    it("name", () => {
        expect(result.name).to.equal(json.name);
    });
    it("school", () => {
        expect(result.school).to.equal(json.school);
    });
    it("subschool", () => {
        expect(result.subSchool).to.equal(json.subSchool);
    });
    it("descriptor", () => {
        expect(result.descriptor).to.equal(json.descriptor);
    });
    it("levels", () => {
        expect(result.levels).deep.equal(json.levels);
    });
    it("domains", () => {
        expect(result.domains).deep.equal(json.domains);
    });
    it("casting time", () => {
        expect(result.castingTime).to.equal(json.castingTime);
    });
    it("components", () => {
        expect(result.components).deep.equal(json.components);
    });
    it("effect", () => {
        expect(result.effect).deep.equal(json.effect);
    });
    it("description", () => {
        expect(result.description).to.equal(json.description);
    });

    if (json.subSpells) {
        describe("Sub spells", () => {
            for (let i = 0; i < json.subSpells.length; i++) {

                let jsonSubSpell = json.subSpells[i];
                let resultSubSpell = result.subSpells ? result.subSpells[i] : {};

                describe(jsonSubSpell.name, () => {
                    it("name", () => {
                        expect(resultSubSpell.name).to.equal(jsonSubSpell.name);
                    });
                    it("school", () => {
                        expect(resultSubSpell.school).to.equal(jsonSubSpell.school);
                    });
                    it("subschool", () => {
                        expect(resultSubSpell.subSchool).to.equal(jsonSubSpell.subSchool);
                    });
                    it("descriptor", () => {
                        expect(resultSubSpell.descriptor).to.equal(jsonSubSpell.descriptor);
                    });
                    it("levels", () => {
                        expect(resultSubSpell.levels).deep.equal(jsonSubSpell.levels);
                    });
                    it("domains", () => {
                        expect(resultSubSpell.domains).deep.equal(jsonSubSpell.domains);
                    });
                    it("casting time", () => {
                        expect(resultSubSpell.castingTime).to.equal(jsonSubSpell.castingTime);
                    });
                    it("components", () => {
                        expect(resultSubSpell.components).deep.equal(jsonSubSpell.components);
                    });
                    it("effect", () => {
                        expect(resultSubSpell.effect).deep.equal(jsonSubSpell.effect);
                    });
                    it("description", () => {
                        expect(resultSubSpell.description).to.equal(jsonSubSpell.description);
                    });
                });
            }
        });
    }

}

function GetSpellData(name) {
    let file = "./suite/spellReaderData/".concat(name);
    return { html: file.concat(".html"), json: file.concat(".json") }
}